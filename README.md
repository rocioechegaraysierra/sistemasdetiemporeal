[![pipeline status](https://gitlab.com/rocioechegaraysierra/sistemasdetiemporeal/badges/master/pipeline.svg)](https://gitlab.com/rocioechegaraysierra/sistemasdetiemporeal/-/commits/master)


Consigna TP Final Sistemas de Tiempo Real
 
Modelar y desarrollar un simulador de sistemas de tiempo real que permita asignar tareas periódicas a un procesador homogéneo. Prestar especial atención a la arquitectura utilizada para modelar los conceptos vistos. Implementar un planificador a elección de los descritos en la teoría. Lenguaje/framework a elección. Elaborar un informe explicando la estructura del proyecto como también las decisiones que se tomaron respecto a la arquitectura. Acudan a la teoría para justificar sus decisiones. Se recomienda agregar ejemplos. El trabajo debe estar acompañado de una forma de corroborar la ejecución de la simulación: por ejemplo utilizando test cases o mostrando un log.

Integrantes: Echegaray Sierra, Rocio
             Gaspari, Waldo

Link al informe: https://docs.google.com/document/d/1ko2p9cz2A8idy7FSunuUNsu1K83atXAecymrUFr_Zt8/edit?usp=sharing