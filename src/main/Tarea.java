/**
 * La tarea debe contener datos de tiempo, computo y deadline.
 * */
public class Tarea{

	private int deadline;
	private int computo;
	private int periodo;

	public Tarea(int periodo, int computo, int deadline){
		this.periodo=periodo;
		this.computo=computo;
		this.deadline=deadline;
	}
	
	public int periodo(){
		return this.periodo;
	}
	
	public void setComputo(int computo) {
		this.computo = computo;
	}

	public int computo(){
		return this.computo;
	}
	
	public int deadline(){
		return this.deadline;
	}
	
	/**
	 * Resta en uno el tiempo de computo al ejecutarse
	 * */
	public void ejecutar(){
		this.setComputo(this.computo - 1);
		System.out.println("Ejecutado. ");
		System.out.print("El computo de la tarea es: ");
		System.out.println(this.computo);
	}
	
}