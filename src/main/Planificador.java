import java.util.LinkedList;

/**
 * Utiliza la planificación cíclica
 */
public class Planificador {
	
	LinkedList<Tarea> tareas = new LinkedList<Tarea>();

	/**
	 * Permite agregar una tarea
	 * */
	public void agregarTarea(Tarea tarea){
		this.tareas.add(tarea);
	}
	
	/**
	 * Indica si hay tareas por realizar
	 * */
	
	public boolean hayTareas(){
		return !(this.tareas.isEmpty());
	}
	
	/**
	 * Devuelve la cantidad de tareas que tiene el Planificador
	 * */
	public int cantidadDeTareas(){
		return this.tareas.size();
	}
	
	/**
	 * Devuelve la cantidad de tareas que tiene el Planificador
	 * */
	public LinkedList<Tarea> obtenerTareas(){
		return tareas;
	}
	
	/**
	 * Verifica la condicion A de marco secundario
	 * */
	public int buscarMayorComputo(){
		int cantidaddetareas=this.tareas.size();
		int mayorComputo = this.tareas.get(0).computo();

		for (int x = 1; x < cantidaddetareas; x++) {
			if (this.tareas.get(x).computo() > mayorComputo) {
				mayorComputo = this.tareas.get(x).computo();
			}
		}
		return mayorComputo;
		
	}
	/**
	 * Se busca el hiperperiodo
	 * */
	public int calcularHiperperiodo(){	
		int hiperperiodo = this.tareas.get(0).periodo();

		for (int x = 1; x < this.tareas.size(); x++) {
			if (this.tareas.get(x).periodo() > hiperperiodo) {
				hiperperiodo = this.tareas.get(x).periodo();
			}
		}
		return hiperperiodo;
		
	}
	
	/**
	 * Se busca el marco secundario
	 * */
	public int calcularMarcoSecundario(){	
		int marcoSecundario = this.tareas.get(0).computo();

		for (int x = 1; x < this.tareas.size(); x++) {
			if (this.tareas.get(x).computo() > marcoSecundario) {
				marcoSecundario = this.tareas.get(x).computo();
			}
		}
		
		while (calcularHiperperiodo() % marcoSecundario != 0) {
			marcoSecundario = marcoSecundario+1;
		}
		return marcoSecundario;			
	}
	
	/**
	 * Verifica la condicion B del marco secundario
	 * */
	public int verificarCondicionB(int tiempoSecundario){
		if(tareas.iterator().next().periodo()% tiempoSecundario == 0){
				return 1;
			}
			tareas.iterator().next();
	
		return 0;
	}
	
	/**
	 * Verifica la condicion C del marco secundario
	 * */
	public boolean verificarCondicionC(int tiempoSecundario){
		boolean resultado = true;
		for (int x = 0; x < this.tareas.size(); x++) {
			if ((2*tiempoSecundario) - (calcularMCD(tiempoSecundario, this.tareas.get(x).periodo())) > this.tareas.get(x).deadline()) {
				resultado = false;
				return resultado;
			}
		}
		return resultado;
	}
	
	private int calcularMCD(int numero1, int numero2){
		while(numero1 != numero2){
			if(numero1 > numero2){
				numero1 = numero1 - numero2;
			} else{
				numero2 = numero2 - numero1;
			}
		}
		return numero1;
	}
		
}