import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class Simulador {
	
	public static void main(String[] args){
		LinkedList<Tarea> tareas = new LinkedList<Tarea>();
		boolean agregarTarea = true;
		InputStream stream = System.in;
		Scanner scanner = new Scanner(stream);
		while(agregarTarea){
			System.out.println("Ingrese el tiempo o periodo de la tarea:");
			String tiempo = scanner.next();
			System.out.println("Ingrese el computo de la tarea:");
			String computo = scanner.next();
			System.out.println("Ingrese el deadline de la tarea:");
			String deadline = scanner.next();
			Tarea tarea = new Tarea(Integer.parseInt(tiempo), Integer.parseInt(computo), Integer.parseInt(deadline));
			tareas.add(tarea);
			System.out.println("Desea agregar otra tarea?");
			String decision = scanner.next();
			if(decision.equals("No")){
				agregarTarea = false;
			}
		}
		Planificador planificador = new Planificador();
		Iterator<Tarea> it = tareas.iterator();
		while(it.hasNext()){
			planificador.agregarTarea(it.next());
		}
		Procesador procesador = new Procesador(planificador.calcularHiperperiodo(), planificador.calcularMarcoSecundario());
		int cantidadDeMarcosSecundarios = procesador.getHiperperiodo() / procesador.getMarcoSecundario();
		System.out.println();
		System.out.println("Comienza ejecución de tareas");
		System.out.println("Hiperperiodo: "+ procesador.getHiperperiodo());
		System.out.println("Marco secundario: " + procesador.getMarcoSecundario());
		
		for(int i = 1; i <= cantidadDeMarcosSecundarios; i++){
			procesador.ejecutarTarea(tareas);
			procesador.setNumeroDeMarcoSecundario(i+1);
		}
		procesador.getTareas();
		procesador.getTareasProcesadas();
		scanner.close();
	}

}