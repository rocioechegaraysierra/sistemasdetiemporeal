import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

public class Procesador {

	private int tiempoDeEjecucion;
	private int hiperperiodo;
	private int marcoSecundario;
	private int marcoSecundarioInicial;
	private int numeroDeMarcoSecundario;
	private int usoProcesador;

	private HashMap <Tarea,Integer> tareasRealizadas;
	private HashMap <Tarea,Integer> tareasProcesadas;
	
	public Procesador(int hiperperiodo, int marcoSecundario){
		this.tiempoDeEjecucion=0;
		this.hiperperiodo=hiperperiodo;
		this.marcoSecundario=marcoSecundario;
		this.marcoSecundarioInicial = marcoSecundario;
		this.numeroDeMarcoSecundario = 1;
		this.usoProcesador=0;
		this.tareasRealizadas = new HashMap <Tarea,Integer> ();
		this.tareasProcesadas = new HashMap <Tarea,Integer> ();
	}
	

	public void getTareas() {
		Iterator<Entry<Tarea,Integer> > iterador = this.tareasRealizadas.entrySet().iterator();
		
		while (iterador.hasNext()) { 
			Map.Entry<Tarea,Integer>  entry = (Map.Entry<Tarea,Integer> )iterador.next();
			Tarea tarea = (Tarea) entry.getKey();
			System.out.println(""+"N de Marco Secundario " + entry.getValue());
			System.out.println(""+"Periodo: " + tarea.periodo());
			System.out.println(""+"Computo: " + tarea.computo());
			System.out.println(""+"Deadline " + tarea.deadline());
		}
	}
	
	public int cantidadDeTareasEjecutadas(){
		return this.tareasRealizadas.size();
	}
		
	public void setHiperperiodo(int hiperperiodo) {
		this.hiperperiodo = hiperperiodo;
	}

	public int getHiperperiodo() {
		return hiperperiodo;
	}

	public void setMarcoSecundario(int marcoSecundario) {
		this.marcoSecundario = marcoSecundario;
	}

	public int getMarcoSecundario() {
		return marcoSecundario;
	}
	
	public int tiempoDeEjecucion() {
		return tiempoDeEjecucion;
	}

	private void setTiempoDeEjecucion(int tiempo) {
		this.tiempoDeEjecucion = tiempo;
	}
	
	public int getNumeroDeMarcoSecundario() {
		return numeroDeMarcoSecundario;
	}
		
	public void setNumeroDeMarcoSecundario(int numeroDeMarcoSecundario) {
		this.numeroDeMarcoSecundario = numeroDeMarcoSecundario;
	}
	
	public void getTareasProcesadas() {
		Iterator<Entry<Tarea,Integer> > iterador = this.tareasProcesadas.entrySet().iterator();
		System.out.println("-");
		System.out.println("Tareas procesadas");
		while (iterador.hasNext()) { 
			Map.Entry<Tarea,Integer>  entry = (Map.Entry<Tarea,Integer> )iterador.next();
			Tarea tarea = (Tarea) entry.getKey();
			System.out.println("-");
			System.out.println(""+"Periodo: " + tarea.periodo());
			System.out.println(""+"Computo: " + tarea.computo());
			System.out.println(""+"Lista en tiempo: " + entry.getValue());
		}
	}
	/**
	 * Agrega a la lista las tareas realizadas
	 * y suma al tiempo de ejecucion el tiempo de la tarea
	 * *
	*/
	public void ejecutarTarea(LinkedList<Tarea> tareas){
			
		Iterator <Tarea> iteradorARealizar = tareas.iterator();
		
		while(iteradorARealizar.hasNext()){
			
			Tarea tarea= iteradorARealizar.next();
			
			int computoDeLaTarea = tarea.computo();
			
			if(!this.tareasProcesadas.containsKey(tarea)){
				procesarTarea(tarea, computoDeLaTarea);
			}else{
				if(this.tiempoDeEjecucion > this.tareasProcesadas.get(tarea)){
					procesarTarea(tarea, computoDeLaTarea);
				}
			}
		}
	
		Tarea tareaYaProcesada = this.buscarTareaProcesada();
		if(tareaYaProcesada != null){
			procesarTarea(tareaYaProcesada, tareaYaProcesada.computo());
		}
	
		while(this.getMarcoSecundario() > 0){
			this.setMarcoSecundario(this.getMarcoSecundario()- 1);
			this.setTiempoDeEjecucion(this.tiempoDeEjecucion()+ 1);
		}
		this.setMarcoSecundario(this.marcoSecundarioInicial);
	}
	
	public void procesarTarea(Tarea tarea, int computoInicialDeLaTarea){
		if(tarea.computo() <= this.getMarcoSecundario()){
			this.tareasRealizadas.put(tarea, this.numeroDeMarcoSecundario);
			this.calcularUso(tarea.computo());
			int computo=tarea.computo();
			System.out.print("Computo: ");
			System.out.println(computo);
			for(int i=1; i<= (computo) ; i++){
				this.setMarcoSecundario(this.getMarcoSecundario()- 1);
				this.setTiempoDeEjecucion(this.tiempoDeEjecucion()+ 1);
				tarea.ejecutar();
				System.out.print("Tiempo de ejecucion: ");
				System.out.println(this.tiempoDeEjecucion());
				System.out.println("-");
			}
			this.tareasProcesadas.put(tarea, this.tiempoDeEjecucion+ tarea.periodo());
			tarea.setComputo(computoInicialDeLaTarea);
			
		}
	}
	
	private void calcularUso(int computo) {
		int porcentaje = 0;
		porcentaje =(computo*100);
		porcentaje = porcentaje/this.getHiperperiodo();
		System.out.print("Uso de procesador la tarea: %");
		System.out.println(porcentaje);
		this.setUsoProcesador(this.getUsoProcesador()+ porcentaje);
		System.out.print("Uso total del procesador: % ");
		System.out.println(this.getUsoProcesador());
		
	}

	public int getUsoProcesador() {
		return usoProcesador;
	}


	private void setUsoProcesador(int usoProcesador) {
		this.usoProcesador = usoProcesador;
	}

	private Tarea buscarTareaProcesada(){
		Tarea tareaEncontrada = null;
		Iterator<Entry<Tarea,Integer> > iterador = this.tareasProcesadas.entrySet().iterator();
		boolean encontroTareaProcesada = false;
		while (iterador.hasNext() & encontroTareaProcesada == false) { 
			Map.Entry<Tarea,Integer>  entry = (Map.Entry<Tarea,Integer> )iterador.next();
			if(entry.getKey().computo() <= this.getMarcoSecundario() & this.tiempoDeEjecucion>=entry.getValue() ){
				encontroTareaProcesada = true;
				tareaEncontrada = entry.getKey();
			}
		}
		return tareaEncontrada;
	}
		
}