import org.junit.Assert;
import org.junit.Test;

public class PlanificadorTest {

	Tarea tarea1 = new Tarea(6,4,6);
	Tarea tarea2 = new Tarea(8,4,8);
	Tarea tarea3 = new Tarea(12,2,12);
	Planificador planificador = new Planificador();
	
	@Test
	public void unaTareaAgregada(){
		planificador.agregarTarea(tarea1);
		Assert.assertEquals(1, planificador.cantidadDeTareas(),0);
	}
	
	@Test
	public void dosTareasAgregadas(){
		planificador.agregarTarea(tarea1);
		planificador.agregarTarea(tarea2);
		Assert.assertEquals(2, planificador.cantidadDeTareas(),0);
	}
	
	@Test
	public void tresTareasAgregadas(){
		planificador.agregarTarea(tarea1);
		planificador.agregarTarea(tarea2);
		planificador.agregarTarea(tarea3);
		Assert.assertEquals(3, planificador.cantidadDeTareas(),0);
	}
	
	@Test
	public void sinHayTareas(){
		Assert.assertEquals(0, planificador.cantidadDeTareas(),0);
	}
	
	@Test
	public void noHayTareas(){
		Assert.assertEquals(false, planificador.hayTareas());
	}
	
	@Test
	public void hayTareas(){
		planificador.agregarTarea(tarea1);
		planificador.agregarTarea(tarea2);
		planificador.agregarTarea(tarea3);
		Assert.assertEquals(true, planificador.hayTareas());
	}
	
	@Test
	public void calculoHiperperiodo(){
		planificador.agregarTarea(tarea1);
		planificador.agregarTarea(tarea2);
		planificador.agregarTarea(tarea3);
		Assert.assertEquals(12, planificador.calcularHiperperiodo(), 0);
	}
	
	@Test
	public void calculoMaximoComputoTresTareas(){
		planificador.agregarTarea(tarea1);
		planificador.agregarTarea(tarea2);
		planificador.agregarTarea(tarea2);
		Assert.assertEquals(4, planificador.buscarMayorComputo(), 0);
	}
	
	@Test
	public void calculoMaximoComputoDosTareas(){
		planificador.agregarTarea(tarea1);
		planificador.agregarTarea(tarea2);
		Assert.assertEquals(4, planificador.buscarMayorComputo(), 0);
	}
	
	
	@Test
	public void existeAlMenosUnoDivididoCeroMarcoSecundarioDosTareas(){
		planificador.agregarTarea(tarea1);
		planificador.agregarTarea(tarea2);
		Assert.assertTrue(planificador.verificarCondicionB(8) ==0);
	}
	
	@Test
	public void existeAlMenosUnoDivididoCeroMarcoSecundarioTresTareas(){
		planificador.agregarTarea(tarea1);
		planificador.agregarTarea(tarea2);
		planificador.agregarTarea(tarea3);
		Assert.assertTrue(planificador.verificarCondicionB(12) ==0);
		
	}
	
	@Test
	public void calculoMarcoSecundarioExacto(){
		planificador.agregarTarea(tarea1);
		planificador.agregarTarea(tarea2);
		planificador.agregarTarea(tarea3);
		Assert.assertEquals(4, planificador.calcularMarcoSecundario(), 0);
	}
	
	@Test
	public void calculoMarcoSecundarioDesplazado(){
		Tarea tarea1 = new Tarea(6,3,6);
		Tarea tarea2 = new Tarea(10,5,10);
		Tarea tarea3 = new Tarea(30,8,30);
		planificador.agregarTarea(tarea1);
		planificador.agregarTarea(tarea2);
		planificador.agregarTarea(tarea3);
		Assert.assertEquals(10, planificador.calcularMarcoSecundario(), 0);
	}
	
	@Test
	public void verificarCondicionCMarcoSecundario(){
		planificador.agregarTarea(tarea1);
		planificador.agregarTarea(tarea2);
		planificador.agregarTarea(tarea3);
		Assert.assertTrue(planificador.verificarCondicionC(planificador.calcularMarcoSecundario()));
	}
	
	@Test
	public void verificarCondicionCMarcoSecundarioDesplazado(){
		Tarea tarea4 = new Tarea(6,3,6);
		Tarea tarea5 = new Tarea(10,5,10);
		Tarea tarea6 = new Tarea(30,6,30);
		planificador.agregarTarea(tarea4);
		planificador.agregarTarea(tarea5);
		planificador.agregarTarea(tarea6);
		Assert.assertTrue(planificador.verificarCondicionC(planificador.calcularMarcoSecundario()));
	}
}
