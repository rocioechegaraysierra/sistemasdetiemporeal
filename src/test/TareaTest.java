import org.junit.Assert;
import org.junit.Test;

public class TareaTest {

	Tarea tarea = new Tarea(4,5,6);
	
	@Test
	public void obtenerValorDeTiempo(){
		Assert.assertEquals(4, tarea.periodo(), 0);
	}
	@Test
	public void obtenerValorDeComputo(){
		Assert.assertEquals(5, tarea.computo(), 0);
	}
	@Test
	public void obtenerValorDeDeadline(){
		Assert.assertEquals(6, tarea.deadline(), 0);
	}
	
	@Test
	public void ejecutarTarea(){
		tarea.ejecutar();
		Assert.assertEquals(4, tarea.computo(), 0);
	}
}