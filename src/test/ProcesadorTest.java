import org.junit.Assert;
import org.junit.Test;

public class ProcesadorTest {

	Tarea tarea1 = new Tarea(5,4,6);
	Tarea tarea2 = new Tarea(9,6,9);
	Tarea tarea3 = new Tarea(12,2,6);
	Tarea tarea4 = new Tarea(2,4,6);
	Tarea tarea5 = new Tarea(6,3,9);
	Tarea tarea6 = new Tarea(6,2,6);
	Tarea tarea7 = new Tarea(4,2,4);
	Tarea tarea8 = new Tarea(8,4,8);
	Planificador planificador = new Planificador();

	@Test
	public void procesaDosTareasDeTresEnUnMarcoSecundario(){		
		planificador.agregarTarea(tarea1);
		planificador.agregarTarea(tarea2);
		planificador.agregarTarea(tarea3);
		Procesador procesador = new Procesador(planificador.calcularHiperperiodo(), planificador.calcularMarcoSecundario());
		procesador.ejecutarTarea(planificador.obtenerTareas());
		Assert.assertEquals(2, procesador.cantidadDeTareasEjecutadas(), 0);
	}
	
	@Test
	public void tiempoDeProcesadorUnaTareaSobreMarcoSecundario(){
		planificador.agregarTarea(tarea5);
		planificador.agregarTarea(tarea3);
		System.out.println(planificador.calcularHiperperiodo());
		System.out.println(planificador.calcularMarcoSecundario());
		Procesador procesador = new Procesador(planificador.calcularHiperperiodo(), planificador.calcularMarcoSecundario());
		procesador.ejecutarTarea(planificador.obtenerTareas());
		Assert.assertEquals(3, procesador.tiempoDeEjecucion(), 0);	
	}
	
	@Test
	public void tiempoDeProcesadorTodasLasTareas(){
		planificador.agregarTarea(tarea4);
		planificador.agregarTarea(tarea3);
		Procesador procesador = new Procesador(planificador.calcularHiperperiodo(), planificador.calcularMarcoSecundario());
		procesador.ejecutarTarea(planificador.obtenerTareas());
		Assert.assertEquals(4, procesador.tiempoDeEjecucion(), 0);
	} 

	@Test
	public void usoDelProcesadorAl50Porciento(){
		Procesador procesador = new Procesador(8,4);
		planificador.agregarTarea(tarea8);
		procesador.ejecutarTarea(planificador.obtenerTareas());
		Assert.assertEquals(50, procesador.getUsoProcesador(), 0);
	}

	@Test
	public void usoDelProcesadorAl33Porciento(){
		Procesador procesador = new Procesador(6, 2);
		planificador.agregarTarea(tarea6);
		planificador.agregarTarea(tarea7);
		procesador.ejecutarTarea(planificador.obtenerTareas());
		Assert.assertEquals(33, procesador.getUsoProcesador(), 0);
	}
	
	@Test
	public void usoDelProcesadorAl6Porciento(){
		Procesador procesador = new Procesador(30, 4);
		planificador.agregarTarea(tarea6);
		procesador.ejecutarTarea(planificador.obtenerTareas());
		Assert.assertEquals(6, procesador.getUsoProcesador(), 0);
	}
}